<html>  
    <head>  
        <title>Gestor de Tareas</title>  
		<link rel="stylesheet" href="jquery-ui.css">
        <link rel="stylesheet" href="bootstrap.min.css" />
		<script src="jquery.min.js"></script>  
		<script src="jquery-ui.js"></script>
    </head>  
    <body>  
        <div class="container">
			<br />	
			<h3 align="center">Gestor de Tareas</a></h3><br />
			<br />
			<div align="left" style="margin-bottom:5px;">
			<button type="button" name="add" id="add" class="btn btn-success btn-xs">Añadir</button>
			</div>
			<div class="table-responsive" id="task_data">
				
			</div>
			<br />
		</div>
		
		<div id="task_dialog" title="Añadir Tarea">
			<form method="post" id="task_form">
	
				<div class="form-group">
					<label>Tarea</label>
					<input type="text" name="tarea" id="tarea" class="form-control" />
					<span id="error_task" class="text-danger"></span>
				</div>
				<label>Categorias:</label>
				<div class="form-group">
					<div>
					<label>JavaScript</label>
					<input type="checkbox" name="categories[]" value="1"/>
					</div>
					<div>
					<label>PHP</label>
					<input type="checkbox" name="categories[]" value="2"/>
					</div>
					<div>
					<label>CSS</label>
					<input type="checkbox" name="categories[]" value="3"/>
					</div>
				</div>
				<div class="form-group">
					<input type="hidden" name="action" id="action" value="insert" />
					<input type="hidden" name="hidden_id" id="hidden_id" />
					<input type="submit" name="form_action" id="form_action" class="btn btn-info" value="Insert" />
				</div>
			</form>
		</div>
		
		<div id="action_alert" title="Action">
			
		</div>
		
		<div id="delete_confirmation" title="Confirmación">
		<p>Estas seguro que desea eliminarlo?</p>
		</div>
		
    </body>  
</html>  




<script>  
$(document).ready(function(){  

	load_data();
    
	function load_data()
	{
		$.ajax({
			url:"table.php",
			method:"POST",
			success:function(data)
			{
				$('#task_data').html(data);
			}
		});
	}
	
	$("#task_dialog").dialog({
		autoOpen:false,
		width:400
	});
	
	$('#add').click(function(){
		$('#task_dialog').attr('title', 'Añadir Tarea');
		$('#action').val('insert');
		$('#form_action').val('Insert');
		$('#task_form')[0].reset();
		$('#form_action').attr('disabled', false);
		$("#task_dialog").dialog('open');
	});
	
	$('#task_form').on('submit', function(event){
		event.preventDefault();
		var error_task = '';
		var error_categories = '';
		if($('#tarea').val() == '')
		{
			error_task = 'Tarea requerida';
			$('#error_task').text(error_task);
			$('#tarea').css('border-color', '#cc0000');
		}
		else
		{
			error_task = '';
			$('#error_task').text(error_task);
			$('#tarea').css('border-color', '');
		}
		if($('#categories').val() == '')
		{
			error_categories = 'categories are required';
			$('#error_categories').text(error_categories);
			$('#categories').css('border-color', '#cc0000');
		}
		else
		{
			error_categories = '';
			$('#error_categories').text(error_categories);
			$('#categories').css('border-color', '');
		}
		
		if(error_task != '' || error_categories != '')
		{
			return false;
		}
		else
		{
			$('#form_action').attr('disabled', 'disabled');
			var form_data = $(this).serialize();
			$.ajax({
				url:"action.php",
				method:"POST",
				data:form_data,
				success:function(data)
				{
					$('#task_dialog').dialog('close');
					$('#action_alert').html(data);
					$('#action_alert').dialog('open');
					load_data();
					$('#form_action').attr('disabled', false);
				}
			});
		}
		
	});
	
	$('#action_alert').dialog({
		autoOpen:false
	});
	
	
	$('#delete_confirmation').dialog({
		autoOpen:false,
		modal: true,
		buttons:{
			Ok : function(){
				var id = $(this).data('id');
				var action = 'delete';
				$.ajax({
					url:"action.php",
					method:"POST",
					data:{id:id, action:action},
					success:function(data)
					{
						$('#delete_confirmation').dialog('close');
						$('#action_alert').html(data);
						$('#action_alert').dialog('open');
						load_data();
					}
				});
			},
			Cancel : function(){
				$(this).dialog('close');
			}
		}	
	});
	
	$(document).on('click', '.delete', function(){
		var id = $(this).attr("id");
		$('#delete_confirmation').data('id', id).dialog('open');
	});
	
});  
</script>