<?php

//fetch.php

include("connection.php");

//Realizamos la consulta sql para obtener la lista de tareas asociadas

$query = 
"SELECT
tareas.id, 
tareas.slug, 
categorias.slug 
FROM cat_assign
INNER JOIN tareas ON tareas.id = cat_assign.tarea_id
INNER JOIN categorias ON  categorias.id = cat_assign.categoria_id
LIMIT 50
";
$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$total_row = $statement->rowCount();
$output = '
<table class="table table-striped table-bordered">
	<tr>
		<th>Tarea</th>
		<th>Categorias</th>
		<th>Borrar</th>
	</tr>
';


//Eliminamos las filas repetidas y agrupamos por categorias.
if($total_row > 0){

	$arrayAux;
	$acum = 0;
	foreach($result as $element){
	
		$arrayAux[$acum] = array(intval($element["id"]));
		$acum = $acum +1;
	}

	$arrayNotDuplicated = eliminarDuplicados($arrayAux);
	$arrayNotDuplicatedAux = $arrayNotDuplicated;
	$acum2 = 0;
	foreach($arrayNotDuplicatedAux as $id){
		$acumId = 1;
		foreach($result as $element){
			if($id[0] == $element["id"]){
				if($acumId == 1){
					$arrayNotDuplicated[$acum2][$acumId] = $element[1];
					$acumId = $acumId + 1;
				}
				$arrayNotDuplicated[$acum2][$acumId] = $element["slug"];
				$acumId = $acumId + 1;
			}

				
		}

	$acum2 = $acum2 + 1;
	}


//Mostramos en la tabla todas las tareas.
	foreach($arrayNotDuplicated as $row)
	{
		$output .= '
		<tr>
			<td width="40%">'.$row[1].'</td>
			<td width="40%">'.$row[2].'';
		
		if(isset($row[3])){		
			if(isset($row[4])){
				$output .=' ,'.$row[3].','.$row[4].'</td>
				<td width="10%">
					<button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$row[0].'">Borrar</button>
				</td>
				</tr>
				';
			}else{
				$output .=' ,'.$row[3].'</td>
				<td width="10%">
					<button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$row[0].'">Borrar</button>
				</td>
				</tr>
				';

			}		
		
		}else{

			$output .='</td>
			<td width="10%">
				<button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$row[0].'">Borrar</button>
			</td>
			</tr>
			';


		}	
			
	}
}
else
{
	$output .= '
	<tr>
		<td colspan="4" align="center">Cree una tarea pulsando en: Añadir</td>
	</tr>
	';
}
$output .= '</table>';
echo $output;




?>


<?php function eliminarDuplicados($array)
{
    $arraySinDuplicados = [];
    foreach($array as $elemento) {
        if (!in_array($elemento, $arraySinDuplicados)) {
            $arraySinDuplicados[] = $elemento;
        }
    }
    return $arraySinDuplicados;
}

?>