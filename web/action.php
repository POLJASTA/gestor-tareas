<?php

//action.php

include('connection.php');

if(isset($_POST["action"]))
{
	if($_POST["action"] == "insert")
	{	
		if(isset($_POST['categories'])){
		//Insertamos la tarea
		$query = "
		INSERT INTO tareas (slug) VALUES ('".$_POST["tarea"]."')
		";
		$statement = $connect->prepare($query);
		$statement->execute();
		
		//recogemos el id de la ultima tarea insertada
		$query = "SELECT id FROM tareas ORDER BY id DESC LIMIT 1";
		$statement = $connect->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		

		//asignamos la tarea a las categorias selecionadas.
		$arr_categories = $_POST['categories'];
	
		foreach($arr_categories as $row){

			$query = "
			INSERT INTO cat_assign (tarea_id, categoria_id) VALUES ('".$result[0]["id"]."','".$row."')
			";
			$statement = $connect->prepare($query);
			$statement->execute();
	
		}

		echo '<p>Datos Insertados...</p>';

		}else{

			echo '<p>Porfavor, selecione al menos una categoria</p>';
		}


		
	}
	

	if($_POST["action"] == "delete")
	{
		//Eliminamos la tarea de la tabla tareas
		$query = "DELETE FROM tareas WHERE id = '".$_POST["id"]."'";
		$statement = $connect->prepare($query);
		$statement->execute();

		//Eliminamos todas las asignaciones de una tarea a categorias.
		$query = "DELETE FROM cat_assign WHERE cat_assign.tarea_id = '".$_POST["id"]."'";
		$statement = $connect->prepare($query);
		$statement->execute();



		echo '<p>Tarea eliminada</p>';
	}
}

?>