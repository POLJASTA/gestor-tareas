Tanto el servidor web como la base de datos se han levantado de manera local con XAMPP en windows. 

Para levantarla debemos tener instalado XAMPP.

- Copiaremos los archivos del proyecto dentro de la ruta C:\xampp\htdocs\web.

- Con XAMPP abierto pulsaremos  "start" en Apache y MySQL.

- Usaremos la url:  http://localhost/phpmyadmin para administrar la BBDD y http://localhost/web/index.php para entrar a la web.

- Dentro de la carpeta "tablas y base de datos" se encuentran las consultas sql para crear las tablas necesarias, ademas de una copia de la BBDD    con los datos que aparezan en las siguientes imagenes mostrando la web.

(Error en la imagen, desde la tabla categorias se une con la tabla cat_assign desde id y no desde slug como aparece)
![Click aqui para ver](https://gitlab.com/POLJASTA/gestor-tareas/-/blob/master/web/img/BBDD%20Diagram.png)

La web se mostraria de la siguiente manera:

![Click aqui para ver](https://gitlab.com/POLJASTA/gestor-tareas/-/blob/master/web/img/index.jpg)

Para añadir una nueva tarea, pulsariamos en "añadir" y nos aparerecia un dialog flotante donde podemos insertar los datos de la tarea. Además podemos eliminar una tarea pulsando en "borrar" que del mismo modo aparecerá un dialog donde nos preguntara si estamos seguro. Todo esto se realiza sin actualizar la pagina.

![Click aqui para ver](https://gitlab.com/POLJASTA/gestor-tareas/-/blob/master/web/img/añadir%20tarea.jpg)


Prueba técnica realizada por: Antonio García de la Costa

